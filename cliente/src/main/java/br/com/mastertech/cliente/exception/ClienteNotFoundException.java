package br.com.mastertech.cliente.exception;

import org.springframework.http.HttpStatus;

public class ClienteNotFoundException extends CartoesException {

    public ClienteNotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}
