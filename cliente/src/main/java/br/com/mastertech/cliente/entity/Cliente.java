package br.com.mastertech.cliente.entity;

import br.com.mastertech.cliente.entity.builder.ClienteBuilder;

import javax.persistence.*;

@Entity
@Table(name="cliente")
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
//    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<Cartao> cartoes = new ArrayList<>();

    public Cliente() {
    }

    public Cliente(String nome) {
        this.nome = nome;
    //    this.cartoes = cartoes;
    }

    public static ClienteBuilder builder() {
        return ClienteBuilder.aCliente();
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

//    public List<Cartao> getCartoes() {
//        return cartoes;
//    }
}
