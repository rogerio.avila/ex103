package br.com.mastertech.cartao.dto;

import javax.validation.constraints.NotNull;

public class AtivaCartaoDto {
    @NotNull(message = "O estado do cartão não pode ser nulo.")
    private boolean ativo;

    public AtivaCartaoDto() {
    }



    public AtivaCartaoDto(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isAtivo() {
        return ativo;
    }
}
