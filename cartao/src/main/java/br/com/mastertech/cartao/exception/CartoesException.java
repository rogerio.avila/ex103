package br.com.mastertech.cartao.exception;

import org.springframework.http.HttpStatus;

class CartoesException extends Exception {
    private HttpStatus httpStatus;

    CartoesException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
