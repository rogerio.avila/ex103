package br.com.mastertech.cartao.dto;

import br.com.mastertech.cartao.dto.builder.CartaoInvalidadoDtoBuilder;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CartaoInvalidadoDto {

    private Long id;

    @NotNull(message = "O Cartao nao pode ser nulo.")
    @NotEmpty(message = "O Cartao nao pode ser vazio.")
    private String numero;


    @Min(value = 1, message = "O id do cliente deve maior que zero.")
    private Long clienteId;

    public CartaoInvalidadoDto(Long id, String numero, Long clienteId, boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
    }

    public static CartaoInvalidadoDtoBuilder builder() {
        return CartaoInvalidadoDtoBuilder.aCartaoSemEstadoDto();
    }

    public Long getId() {
        return id;
    }

    public String getNumero() {
        return numero;
    }

    public Long getClienteId() {
        return clienteId;
    }
}
