package br.com.mastertech.cartao.entity;

import br.com.mastertech.cartao.entity.builder.CartaoBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "cartao")
public class Cartao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "numero")
    private String numero;
    @Column(name = "ativo")
    private boolean ativo = Boolean.FALSE;
    @Column(name = "cliente_id")
    private long clienteId;
//    @OneToMany(mappedBy = "cartao", cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<Pagto> pagtos = new ArrayList<>();

    public Cartao() {
    }

    public Cartao(Long id, String numero, boolean ativo, long clienteId) {
        this.id = id;
        this.numero = numero;
        this.ativo = ativo;
        this.clienteId = clienteId;
      //  this.pagtos = pagtos;
    }

    public static CartaoBuilder builder() {
        return CartaoBuilder.aCartao();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public long getClienteId() {
        return clienteId;
    }

    public void setClienteId(long clienteId) {
        this.clienteId = clienteId;
    }

//    public List<Pagto> getPagtos() {
//        return pagtos;
//    }
//
//    public void setPagtos(List<Pagto> pagtos) {
//        this.pagtos = pagtos;
//    }
}
