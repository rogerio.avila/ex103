package br.com.mastertech.cartao.client;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Cliente {

    private Long id;
    @NotNull(message = "O nome do cliente nao pode ser nulo.")
    @NotEmpty(message = "O nome do cliente nao pode ser vazio.")
    private String nome;

    public Cliente(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

}
