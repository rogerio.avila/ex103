package br.com.mastertech.cartao.dto.builder;

import br.com.mastertech.cartao.dto.CartaoInvalidadoDto;

public final class CartaoInvalidadoDtoBuilder {
    private Long id;
    private String numero;
    private Long clienteId;


    private CartaoInvalidadoDtoBuilder() {
    }



    public static CartaoInvalidadoDtoBuilder aCartaoSemEstadoDto() {
        return new CartaoInvalidadoDtoBuilder();
    }

    public CartaoInvalidadoDtoBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public CartaoInvalidadoDtoBuilder numero(String numero) {
        this.numero = numero;
        return this;
    }

    public CartaoInvalidadoDtoBuilder clienteId(Long clienteId) {
        this.clienteId = clienteId;
        return this;
    }



    public CartaoInvalidadoDto build() {
        return new CartaoInvalidadoDto(id, numero, clienteId, false);
    }
}
