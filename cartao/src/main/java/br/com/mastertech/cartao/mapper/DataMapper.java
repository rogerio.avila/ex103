package br.com.mastertech.cartao.mapper;

import br.com.mastertech.cartao.dto.CartaoDto;
import br.com.mastertech.cartao.dto.CartaoInvalidadoDto;
import br.com.mastertech.cartao.entity.Cartao;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DataMapper {
    DataMapper INSTANCE = Mappers.getMapper(DataMapper.class);

//    Cliente clienteDtoToCliente(ClienteDto clienteDto);
//    ClienteDto clienteToClienteDto(Cliente cliente);
//    List<ClienteDto> clienteToClienteDto(List<Cliente> cliente);

    Cartao cartaoDtoToCartao(CartaoDto cartaoDto);
    @Mapping(target = "clienteId", source = "clienteId")
    CartaoDto cartaoToCartaoDto(Cartao cartao);
    @Mapping(target = "clienteId", source = "clienteId")
    CartaoInvalidadoDto cartaoToCartaoSemEstadoDto(Cartao cartao);
    List<CartaoDto> cartaoToCartaoDto(List<Cartao> cartao);

//    Pagto pagtoDtoToPagto(PagtoDto pagtoDto);
//    @Mapping(target = "cartaoId", source = "cartao.id")
//    PagtoDto pagtoToPagtoDto(Pagto pagto);
//    List<PagtoDto> pagtoToPagtoDto(List<Pagto> pagto);
}
