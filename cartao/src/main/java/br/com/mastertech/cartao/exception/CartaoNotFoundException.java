package br.com.mastertech.cartao.exception;

import org.springframework.http.HttpStatus;

public class CartaoNotFoundException extends CartoesException {
    public CartaoNotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}
