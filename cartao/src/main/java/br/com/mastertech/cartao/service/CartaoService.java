package br.com.mastertech.cartao.service;

import br.com.mastertech.cartao.client.Cliente;
import br.com.mastertech.cartao.client.ClienteClient;
import br.com.mastertech.cartao.entity.Cartao;
import br.com.mastertech.cartao.exception.CartaoNotFoundException;
import br.com.mastertech.cartao.exception.ClienteNotFoundException;
import br.com.mastertech.cartao.repository.CartaoRepository;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CartaoService {
    private final CartaoRepository cartaoRepository;
    private final ClienteClient clienteRepository;

    public CartaoService(CartaoRepository cartaoRepository, ClienteClient clienteRepository) {
        this.cartaoRepository = cartaoRepository;
        this.clienteRepository = clienteRepository;
    }

    public List<Cartao> findAll() {
        Iterable<Cartao> all = cartaoRepository.findAll();
        return StreamSupport.stream(all.spliterator(), false).collect(Collectors.toList());
    }

    public Cartao save(Long clienteId, Cartao cartao) throws ClienteNotFoundException {
        Cliente clientById = clienteRepository.getCliente(clienteId);
        cartao.setClienteId(clientById.getId());
        return cartaoRepository.save(cartao);
    }

    public Cartao ativacaoCartao(String numeroCartao, boolean ativo) throws CartaoNotFoundException {
        Cartao cartao = getCartaoByNumeroImpl(numeroCartao);
        cartao.setAtivo(ativo);
        return cartaoRepository.save(cartao);
    }

    public Cartao findByNumero(String numeroCartao) throws CartaoNotFoundException {
        return getCartaoByNumeroImpl(numeroCartao);
    }

    private Cartao getCartaoByNumeroImpl(String numeroCartao) throws CartaoNotFoundException {
        Optional<Cartao> cartaoByNumero = cartaoRepository.findByNumero(numeroCartao);
        if (!cartaoByNumero.isPresent()) {
            throw new CartaoNotFoundException(MessageFormat.format("O Cartão {0} não encontrado !!", numeroCartao));
        }
        return cartaoByNumero.get();
    }

    public Cartao findbyId(Long idCartao) throws CartaoNotFoundException {
        Optional<Cartao> cartaoByNumero = cartaoRepository.findById(idCartao);
        if (!cartaoByNumero.isPresent()) {
            throw new CartaoNotFoundException(MessageFormat.format("O Cartão {0} não encontrado !!", idCartao));
        }
        return cartaoByNumero.get();
    }
}
