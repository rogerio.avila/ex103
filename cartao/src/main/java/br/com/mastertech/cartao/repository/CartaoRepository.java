package br.com.mastertech.cartao.repository;

import br.com.mastertech.cartao.entity.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Long> {
    Optional<Cartao> findByNumero(String numeroCartao);
}
