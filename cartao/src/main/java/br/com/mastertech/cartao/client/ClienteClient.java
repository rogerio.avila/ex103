package br.com.mastertech.cartao.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@FeignClient(name="Cliente")
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    Cliente getCliente(@Valid
                                     @Min(value = 1, message = "O id do cliente deve ser maior que 0.")
                                     @PathVariable("id") Long clienteId) ;
}
