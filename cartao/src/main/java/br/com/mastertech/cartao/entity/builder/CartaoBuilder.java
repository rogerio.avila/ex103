package br.com.mastertech.cartao.entity.builder;

import br.com.mastertech.cartao.entity.Cartao;

public final class CartaoBuilder {
    private Long id;
    private String numero;
    private boolean ativo = Boolean.FALSE;
  //  private List<Pagto> pagtos = new ArrayList<>();

    private CartaoBuilder() {
    }

    public static CartaoBuilder aCartao() {
        return new CartaoBuilder();
    }

    public CartaoBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public CartaoBuilder numero(String numero) {
        this.numero = numero;
        return this;
    }

    public CartaoBuilder ativo(boolean ativo) {
        this.ativo = ativo;
        return this;
    }

//    public CartaoBuilder pagtos(List<Pagto> pagtos) {
//        this.pagtos = pagtos;
//        return this;
//    }

    public Cartao build() {
        Cartao cartao = new Cartao();
        cartao.setId(id);
        cartao.setNumero(numero);
        cartao.setAtivo(ativo);
       // cartao.setPagtos(pagtos);
        return cartao;
    }
}
