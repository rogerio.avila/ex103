package br.com.mastertech.pagamento.exception;

import org.springframework.http.HttpStatus;

public class CartaoNotFoundException extends PagtoException {
    public CartaoNotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}
