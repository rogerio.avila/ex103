package br.com.mastertech.pagamento.client;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

public class Cartao {

    private Long id;
    private String numero;
    private boolean ativo = Boolean.FALSE;
    private Long clienteId;

    public Cartao(Long id, String numero, boolean ativo, Long clienteId) {
        this.id = id;
        this.numero = numero;
        this.ativo = ativo;
        this.clienteId = clienteId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }
}
