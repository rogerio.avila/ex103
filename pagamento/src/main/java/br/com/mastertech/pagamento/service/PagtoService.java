package br.com.mastertech.pagamento.service;

import br.com.mastertech.pagamento.client.Cartao;
import br.com.mastertech.pagamento.client.CartaoClient;

import br.com.mastertech.pagamento.entity.Pagto;
import br.com.mastertech.pagamento.exception.CartaoNotFoundException;
import br.com.mastertech.pagamento.repository.PagtoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagtoService {

    private final PagtoRepository pagtoRepository;
    private final CartaoClient cartaoRepository;


    public PagtoService(PagtoRepository pagtoRepository, CartaoClient cartaoRepository) {
        this.pagtoRepository = pagtoRepository;
        this.cartaoRepository = cartaoRepository;
    }

    public Pagto save(Long cartaoId, Pagto pagto) throws CartaoNotFoundException {
        Cartao cartao = getCartaoImpl(cartaoId);
        pagto.setCartao(cartaoId);
        return pagtoRepository.save(pagto);
    }


    public List<Pagto> getPagtosPorCartao(Long cartaoId) throws CartaoNotFoundException {
        Cartao cartao = getCartaoImpl(cartaoId);
        return pagtoRepository.findAllByCartaoId(cartaoId);
    }

    private Cartao getCartaoImpl(Long cartaoId) throws CartaoNotFoundException {
        return cartaoRepository.getCartaobyId(cartaoId);
    }
}
