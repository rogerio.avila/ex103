package br.com.mastertech.pagamento.entity.builder;

import br.com.mastertech.pagamento.entity.Pagto;

public final class PagtoBuilder {
    private String descricao;
    private double valor;
    // @ManyToOne(fetch = FetchType.LAZY)
     private Long cartaoId;

    private PagtoBuilder() {
    }

    public static PagtoBuilder aPagto() {
        return new PagtoBuilder();
    }

    public PagtoBuilder descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public PagtoBuilder valor(double valor) {
        this.valor = valor;
        return this;
    }

    public PagtoBuilder cartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
        return this;
    }

    public Pagto build() {
        return new Pagto(descricao, valor, cartaoId);
    }
}
