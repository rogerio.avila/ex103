package br.com.mastertech.pagamento.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="Cartao")
public interface CartaoClient {
    @GetMapping("/cartao/id/{id}")
    Cartao getCartaobyId(
            @PathVariable("id") Long idCartao);
}
