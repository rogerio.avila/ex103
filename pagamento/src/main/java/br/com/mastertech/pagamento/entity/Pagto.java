package br.com.mastertech.pagamento.entity;

import br.com.mastertech.pagamento.entity.builder.PagtoBuilder;

import javax.persistence.*;

@Entity
@Table(name = "pagto")
public class Pagto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "valor")
    private double valor;
   // @ManyToOne(fetch = FetchType.LAZY)
    @Column(name = "cartao_id")
    private Long cartaoId;

    public Pagto() {
    }

    public Pagto(String descricao, double valor, Long cartaoId) {
        this.descricao = descricao;
        this.valor = valor;
        this.cartaoId = cartaoId;
    }

    public static PagtoBuilder builder() {
        return PagtoBuilder.aPagto();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public double getValor() {
        return valor;
    }

    public Long getCartaoId() {
        return cartaoId;
    }

    public void setCartao(Long cartaoId) {
        this.cartaoId = cartaoId;
    }
}
