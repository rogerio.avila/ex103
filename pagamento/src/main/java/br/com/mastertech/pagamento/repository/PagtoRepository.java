package br.com.mastertech.pagamento.repository;

//import br.com.mastertech.pagamento.entity.Cartao;
import br.com.mastertech.pagamento.entity.Pagto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagtoRepository extends CrudRepository<Pagto, Long> {
    List<Pagto> findAllByCartaoId(Long cartaoId);
}
