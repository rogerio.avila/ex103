package br.com.mastertech.pagamento.dto;

import br.com.mastertech.pagamento.dto.builder.PagtoDtoBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PagtoDto {

    private Long id;

    @NotNull(message = "A descrição do pagamento nao pode ser nula.")
    @NotEmpty(message = "A descrição do pagamento nao pode ser vazia.")
    private String descricao;

    @DecimalMin(value = "0.01", message = "O valor da transação deve ser maior que zero.")
    private double valor;


    @Min(value = 1, message = "O id do cartão deve ser maior que zero.")
    @JsonProperty("cartao_id")
    private Long cartaoId;

    public PagtoDto(Long id, String descricao, double valor, Long cartaoId) {
        this.id = id;
        this.descricao = descricao;
        this.valor = valor;
        this.cartaoId = cartaoId;
    }

    public static PagtoDtoBuilder builder() {
        return PagtoDtoBuilder.aPagtoDto();
    }

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public double getValor() {
        return valor;
    }

    public Long getCartaoId() {
        return cartaoId;
    }
}
