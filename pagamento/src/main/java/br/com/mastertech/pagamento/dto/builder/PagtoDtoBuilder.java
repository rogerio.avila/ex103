package br.com.mastertech.pagamento.dto.builder;

import br.com.mastertech.pagamento.dto.PagtoDto;

public final class PagtoDtoBuilder {
    private Long id;
    private String descricao;
    private double valor;
    private Long cartaoId;

    private PagtoDtoBuilder() {
    }



    public static PagtoDtoBuilder aPagtoDto() {
        return new PagtoDtoBuilder();
    }

    public PagtoDtoBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public PagtoDtoBuilder descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public PagtoDtoBuilder valor(double valor) {
        this.valor = valor;
        return this;
    }

    public PagtoDtoBuilder cartaoId(Long cartaoId) {
        this.cartaoId = cartaoId;
        return this;
    }



    public PagtoDto build() {
        return new PagtoDto(id, descricao, valor, cartaoId);
    }
}
