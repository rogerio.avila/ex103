package br.com.mastertech.pagamento.exception;

import org.springframework.http.HttpStatus;

public class PagtoException extends Exception{
    private HttpStatus httpStatus;

    PagtoException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
